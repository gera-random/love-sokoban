function love.load()
    love.graphics.setBackgroundColor(1, 1, .75)

    require 'levels'

    currentLevel = 1
    levelWidth = 0
    levelHeight = 0
    moveCountRecord = 0
    RECORD_FILE = "records.txt"
    recordData = {}
    love.keyboard.setKeyRepeat(true)
    countersVisible = true
    marksVisible = false

    function readRecordData()
        if love.filesystem.getInfo(RECORD_FILE) then
            for line in love.filesystem.lines(RECORD_FILE) do
                vals = {}
                for val in line:gmatch("[^,]+") do
                    table.insert(vals, tonumber(val))
                end
                local level, record = unpack(vals)
                recordData[level] = record
            end
        end
    end

    function writeRecordData()
        local data = ""
        for level, record in pairs(recordData) do
            data = data .. tostring(level) .. "," .. tostring(record) .. "\n"
        end
        love.filesystem.write(RECORD_FILE, data)
    end
    
    DesiredWidth = 1000
    DesiredHeight = 1000
    DesiredCellSize = 100

    LogicalScale = {
        -- Current screen dimension
        ScreenX = DesiredWidth,
        ScreenY = DesiredHeight,
        -- Letterboxing offset
        OffX = 0,
        OffY = 0,
        -- Overall letterboxing scaling
        ScaleOverall = 1
    }

    function recalcLogicalScale(w, h)
        if w == nil or h == nil then
            w, h = love.graphics.getDimensions()
        end

        LogicalScale.ScreenX, LogicalScale.ScreenY = w, h
        LogicalScale.ScaleOverall = math.min(
            LogicalScale.ScreenX / DesiredWidth,
            LogicalScale.ScreenY / DesiredHeight
        )
        LogicalScale.OffX = (LogicalScale.ScreenX - LogicalScale.ScaleOverall * DesiredWidth) / 2
        LogicalScale.OffY = (LogicalScale.ScreenY - LogicalScale.ScaleOverall * DesiredHeight) / 2
    end

    function loadLevel()
        level = {}
        for y, row in ipairs(levels[currentLevel]) do
            level[y] = {}
            for x, cell in ipairs(row) do
                level[y][x] = cell
            end
        end
        levelWidth, levelHeight = levelDimensions()

        DesiredWidth = DesiredCellSize * levelWidth
        DesiredHeight = DesiredCellSize * levelHeight
        recalcLogicalScale()

        moves = {}
        moveCount = 0

        readRecordData()
        if recordData[currentLevel] then
            moveCountRecord = recordData[currentLevel]
        else
            moveCountRecord = 0
        end
    end

    function levelDimensions()
        local height = #level
        local width = 0
        for i = 1, height do
            local rowLength = #level[i]
            if rowLength > width then
                width = rowLength
            end
        end
        return width, height
    end

    function locatePlayer()
        local playerX
        local playerY

        for testY, row in ipairs(level) do
            for testX, cell in ipairs(row) do
                if cell == player or cell == playerOnStorage then
                    playerX = testX
                    playerY = testY
                end
            end
        end

        return playerX, playerY
    end

    loadLevel()

    player = '@'
    playerOnStorage = '+'
    box = '$'
    boxOnStorage = '*'
    storage = '.'
    wall = '#'
    empty = ' '

    function boxCannotMove(x, y)
        local function isObstacle(x, y)
            return level[y][x] == wall or level[y][x] == box or level[y][x] == boxOnStorage
        end
        return (isObstacle(x-1, y) and isObstacle(x, y-1))
            or (isObstacle(x, y-1) and isObstacle(x+1, y))
            or (isObstacle(x+1, y) and isObstacle(x, y+1))
            or (isObstacle(x, y+1) and isObstacle(x-1, y))
    end

    moves = {}
    moveCount = 0

    fontSmall = love.graphics.newFont(15)
    fontLarge = love.graphics.newFont(64 * LogicalScale.ScaleOverall)

    do
        local nextAdjacent = {
            [empty] = player,
            [storage] = playerOnStorage,
        }
        local nextCurrent = {
            [player] = empty,
            [playerOnStorage] = storage,
        }
        local nextBeyond = {
            [empty] = box,
            [storage] = boxOnStorage,
        }
        local nextAdjacentPush = {
            [box] = player,
            [boxOnStorage] = playerOnStorage,
        }

        function makeMove(playerX, playerY, dx, dy, current, adjacent, beyond)
            local moved = false

            if nextAdjacent[adjacent] then
                level[playerY][playerX] = nextCurrent[current]
                level[playerY + dy][playerX + dx] = nextAdjacent[adjacent]
                moved = true
            elseif nextBeyond[beyond] and nextAdjacentPush[adjacent] then
                level[playerY][playerX] = nextCurrent[current]
                level[playerY + dy][playerX + dx] = nextAdjacentPush[adjacent]
                level[playerY + dy + dy][playerX + dx + dx] = nextBeyond[beyond]
                moved = true
            end

            if moved then
                moveCount = moveCount + 1
                moves[moveCount] = {}
                moves[moveCount].dx = dx
                moves[moveCount].dy = dy
                moves[moveCount].current = current
                moves[moveCount].adjacent = adjacent
                moves[moveCount].beyond = beyond
            end
        end

        function undoMove(playerX, playerY)
            local move = moves[moveCount]

            if move then
                level[playerY - move.dy][playerX - move.dx] = move.current
                level[playerY][playerX] = move.adjacent
                level[playerY + move.dy][playerX + move.dx] = move.beyond

                moveCount = moveCount - 1
            end
        end

        function redoMove(playerX, playerY)
            local move = moves[moveCount + 1]

            if move then
                makeMove(playerX, playerY, move.dx, move.dy, move.current, move.adjacent, move.beyond)
            end
        end
    end
end

function love.resize(w, h)
    recalcLogicalScale(w, h)
    local newFontSize = 64 * LogicalScale.ScaleOverall
    if newFontSize > 4 then
        fontLarge = love.graphics.newFont(newFontSize)
    end
end

function love.keypressed(key)
    if key == 'up' or key == 'down' or key == 'left' or key == 'right'
        or key == 'h' or key == 'j' or key == 'k' or key == 'l' then
        local playerX, playerY = locatePlayer()

        local dx = 0
        local dy = 0
        if key == 'left' or key == 'h' then
            dx = -1
        elseif key == 'right' or key == 'l' then
            dx = 1
        elseif key == 'up' or key == 'k' then
            dy = -1
        elseif key == 'down' or key == 'j' then
            dy = 1
        end

        local current = level[playerY][playerX]
        local adjacent = level[playerY + dy][playerX + dx]
        local beyond
        if level[playerY + dy + dy] then
            beyond = level[playerY + dy + dy][playerX + dx +dx]
        end

        makeMove(playerX, playerY, dx, dy, current, adjacent, beyond)
        moves[moveCount + 1] = nil -- truncate redo

        local complete = true

        for y, row in ipairs(level) do
            for x, cell in ipairs(row) do
                if cell == box then
                    complete = false
                end
            end
        end

        if complete then
            if not recordData[currentLevel] or recordData[currentLevel] > moveCount then
                recordData[currentLevel] = moveCount
                writeRecordData()
            end
            currentLevel = currentLevel + 1
            if currentLevel > #levels then
                currentLevel = 1
            end
            loadLevel()
        end
    elseif key == 'u' then
        local playerX, playerY = locatePlayer()
        undoMove(playerX, playerY)
    elseif key == 'i' then
        local playerX, playerY = locatePlayer()
        redoMove(playerX, playerY)
    elseif key == 'r' then
        loadLevel()
    elseif key == 'n' then
        currentLevel = currentLevel + 1
        if currentLevel > #levels then
            currentLevel = 1
        end
        loadLevel()
    elseif key == 'p' then
        currentLevel = currentLevel - 1
        if currentLevel < 1 then
            currentLevel = #levels
        end
        loadLevel()
    elseif key == 'f1' then
        countersVisible = not countersVisible
    elseif key == 'f2' then
        love.mouse.setVisible(not love.mouse.isVisible())
    elseif key == 'f3' then
        marksVisible = not marksVisible
    end
end

function love.draw()
    love.graphics.push("all")

    love.graphics.translate(LogicalScale.OffX, LogicalScale.OffY)
    love.graphics.scale(LogicalScale.ScaleOverall, LogicalScale.ScaleOverall)

    local cellWidth = DesiredCellSize
    local cellHeight = DesiredCellSize

    for y, row in ipairs(level) do
        for x, cell in ipairs(row) do
            if cell ~= ' ' then
                local colors = {
                    [player] = {.64, .53, 1},
                    [playerOnStorage] = {.62, .47, 1},
                    [box] = {1, .79, .49},
                    [boxOnStorage] = {.59, 1, .5},
                    [storage] = {.61, .9, 1},
                    [wall] = {1, .58, .82},
                }

                love.graphics.setColor(colors[cell])
                if cell == box and boxCannotMove(x, y) then
                    love.graphics.setColor(1, .75, .45)
                elseif cell == boxOnStorage and boxCannotMove(x, y) then
                    love.graphics.setColor(.52, 1, .42)
                end

                love.graphics.rectangle(
                    'fill',
                    (x - 1) * cellWidth,
                    (y - 1) * cellHeight,
                    cellWidth,
                    cellHeight
                )

                if marksVisible then
                    love.graphics.setColor(1, 1, 1)
                    love.graphics.setFont(fontSmall)
                    love.graphics.print(
                        level[y][x],
                        (x - 1) * cellWidth,
                        (y - 1) * cellHeight
                    )
                end
            end
        end
    end

    love.graphics.pop()

    if countersVisible then
        love.graphics.setFont(fontLarge)
        love.graphics.setColor(0.5, 0.125, 0)
        local screenW, screenH = love.graphics.getDimensions()
        local movesStr = tostring(moveCount)
        if moveCountRecord > 0 then
            movesStr = movesStr .. "/" .. moveCountRecord
        end
        love.graphics.print(movesStr, screenW, screenH, 0, 1, 1,
            fontLarge:getWidth(movesStr) + fontLarge:getWidth(" ")/2, fontLarge:getHeight(movesStr))
        love.graphics.setColor(0.7, 0.65, 0.25, 0.4)
        local currentLevelStr = tostring(currentLevel)
        love.graphics.print(currentLevelStr, 0, screenH, 0, 1, 1,
            -fontLarge:getWidth(" ")/2, fontLarge:getHeight(currentLevelStr))
    end
end