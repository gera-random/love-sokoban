function love.conf(t)
    t.window.title = "Sokoban"
    t.window.resizable = true
    t.version = "11.5"
    t.modules.audio = false
    t.modules.data = false
    t.modules.image = false
    t.modules.joystick = false
    t.modules.math = false
    t.modules.physics = false
    t.modules.sound = false
    t.modules.thread = false
    t.modules.timer = false
    t.modules.touch = false
end